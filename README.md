Projet web DUT informatique 2a
Julien Soares
Jules Velluet
Nicolas Odiot
Romain Cabrera
Killian Gaurin

Commandes :

$ virtualenv -p python3 venv
$ source venv/bin/activate

A noter que les commandes ci-dessus changent si vous êtes sur windows ou linux

Se déplacer dans le repertoire du projet que vous avez dézipé

$ pip install -r requirement.txt
$ flask loaddb application/data.yaml
$ flask run

aller sur : http://127.0.0.0:5000/

Nous avons choisie de ne pas activer la page register
donc pour ce connecter au différents rôles :

administrateurs :
  Mail :
    nicolatodiot@gmail.com
  mdp :
    admin

professeur :
  mail :
    julesvelluet@gmail.com
  mdp :
    professeur

etudiant:
  mail :
    juliensoares@gmail.com
  mdp :
    etudiant


