# -*- coding: utf-8 -*-

from .app import app
from flask import render_template
from flask import request
from .models import *
from .modelsProf import *
from .modelsEtu import *
from .modelsAdmin import *
from flask import url_for, redirect, flash
from .sheets import recup
from .xlsx import load_xlsx
from flask_wtf import FlaskForm
from flask_uploads import IMAGES, DOCUMENTS, UploadSet, configure_uploads, ALL
from werkzeug.utils import secure_filename
from wtforms import StringField, HiddenField,PasswordField, TextAreaField, SelectField, TimeField, IntegerField, FileField
from wtforms.validators import DataRequired, NumberRange
from hashlib import sha256
from flask_login import login_user , current_user, logout_user, login_required
from wtforms.validators import DataRequired, NumberRange
from werkzeug.utils import secure_filename
import json


@app.route("/")
def home():
    return render_template(
        "home.html",
        title = "EscaMath Home",
        moyenne = moyenne_des_votes()
    )

@app.route("/avis")
def avis():
    return render_template(
        "avis.html",
        title = "EscaMath avis",
        avis = get_avis(),
        user = get_user("juliensoares@gmail.com"),
        moyenne = moyenne_des_votes()
    )

@app.route("/about")
def about():
    return render_template(
        "about.html",
        title = "EscaMath about",
        moyenne = moyenne_des_votes()
    )

class CreerGroupeForm(FlaskForm):
    nom = StringField("Nom", validators=[DataRequired()])

@app.route("/etudiant")
@login_required
def escapeGame():
    return render_template(
        "escapeGame.html",
        title = "EscaMath escape game",
        escapes = get_scenario_accessible(current_user.mail),
        groupes = get_groupe(current_user.mail)
    )

@app.route("/etudiant/jeu", methods=("POST",))
@login_required
def jeu():
    heure = int(request.form.get("heureRestante"))
    minute = int(request.form.get("minuteRestante"))
    seconde = int(request.form.get("secondeRestante"))
    escape_id = int(request.form.get("escape_id"))
    try:
        groupe = int(request.form.get("groupe"))
    except:
        return redirect(url_for("escapeGame"))

    if(request.form.get("newPartie")):
        partie = newPartie(request.form.get("etudiant_id"),heure,minute,seconde,escape_id,groupe)
    else:
        partie = get_partie(request.form.get("id_partie"))
        mise_a_jour_temps(partie.id,heure,minute,seconde)

    enig_id = request.form.get("enigme_id")

    if( enig_id != None):
        incremente_essaie(enig_id,partie.id)
        set_reponse_donnee(enig_id, partie.id, request.form.get("reponseDonnee"))
        if(request.form.get("reponseDonnee") == get_enigme(int(enig_id)).reponse):
            resultat = "Bravo !"
        else:
            resultat = "ET NON"
    else:
        resultat = "Pas de réponse"

    all_enigmes_scenario = get_enigmes_scenario(escape_id)
    all_enigmes = get_enigme_salle(int(request.form.get("salle_id")))
    enigme_non_resolu = []
    enigmes = []
    for enigme in all_enigmes_scenario:
        if get_reponse_donnee(enigme.enigme.id, partie.id) != enigme.enigme.reponse:
            if enigme in all_enigmes:
                enigmes.append(enigme)
            enigme_non_resolu.append(enigme)

    for enigme in list(enigme_non_resolu):
        if get_essaie(enigme.enigme.id,partie.id) == 3:
            if enigme in enigmes:
                enigmes.remove(enigme)
            enigme_non_resolu.remove(enigme)

    salle_finit = False
    if enigmes == []:
        salle_finit = True

    partie_finit = False
    if enigme_non_resolu == []:
        partie_finit = True
        point_total = 0
        point_obtenu = 0
        for enigme in all_enigmes_scenario:
            point_total += enigme.enigme.points
            if get_reponse_donnee(enigme.enigme.id, partie.id) == enigme.enigme.reponse:
                point_obtenu += enigme.enigme.points/get_essaie(enigme.enigme.id,partie.id)
        note = round((point_obtenu/point_total) * 20,2)
    else:
        note = 3

    if (heure == 0) and (minute == 0) and (seconde == 0):
        partie_finit = True

    print(int(request.form.get("salle_id")))

    return render_template(
        "jeu.html",
        title = "EscaMath Jeu",
        premiere_salle = get_scenario(escape_id).salle.id,
        id_salle = int(request.form.get("salle_id")),
        intro = request.form.get("intro"),
        description = (get_introduction(escape_id)),
        salle_direction = get_direction(escape_id),
        heure = partie.timer.hour,
        minute = partie.timer.minute,
        seconde = partie.timer.second,
        enigmes = enigmes,
        resultat = resultat,
        partie = partie.id,
        salle_finit = salle_finit,
        fin = partie_finit,
        note = note,
        groupe = groupe,
        escape_id = escape_id
    )

@app.route("/etudiant/escaperealises")
@login_required
def escapeRealises():
    parties = get_parties_realises(current_user.mail)
    note = {}
    for partie in parties:
        note[partie.id] = get_note(partie.id,current_user.mail)
    return render_template(
        "escapeRealises.html",
        title = "EscaMath Escape Réalisés",
        parties = parties,
        note = note,
        escapes = get_scennario_realiser(current_user.mail),
        moyenne = get_note_scena(current_user.mail),
        moyenne_total = round(moyenne_total(current_user.mail),2),
        nb_escape_realise = len(get_note_scena(current_user.mail)),
        eleve_groupe = get_etu_groupe()
    )

class Exelform(FlaskForm):
    file = FileField("excel")

photos = UploadSet("photos",ALL)
configure_uploads(app, photos)
@app.route("/professeur", methods=['get','post'])
@login_required
def professeurEleve():
    form = Exelform()
    if request.method == "POST":
        if request.form.get('classe') != None:
            classeCreer = request.form['classe']
            promo = request.form['promotion']
            new_classe(classeCreer,promo)
        elif request.form.get('nomClasse') != None:
            classeEleve = request.form['nomClasse']
            nomEleve = request.form['nomEleve']
            mailEleve = request.form['email']
            promo = request.form['promotion']
            add_eleve(classeEleve, nomEleve, mailEleve,promo)
        elif request.form.get('nom') != None:
            nom = request.form['nom']
            prenom = request.form['prenom']
            email = request.form['email']
            promo = request.form['promo']
            classe = request.form['classe_eleve']
            mdp = request.form['mdp']
            new_eleve(nom,prenom,email,mdp,promo,classe)
        elif request.form.get('mailsupp') != None:
            email = request.form['mailsupp']
            supp_eleve(email)
        elif form.validate_on_submit():
            filename = photos.save(form.file.data)
            student = load_xlsx(filename)
        elif request.form.get('valid') != None:
            eleves = recup()
            ajout_eleve_classe(eleves)
        elif request.form.get('classesupp') != None:
            classe = request.form['classesupp']
            supp_classe(classe)
        elif request.form.get('ajoutpromo') != None:
            promo = request.form['ajoutpromo']
            add_promo(promo)

    classe = request.form.get("classons")
    return render_template(
        "professeurEleve.html",
        title = "Professeur éléve",
        prof = get_prof_nom(),
        dict_etu_classe = get_etu_classe(),
        f = form,
        groupes = get_all_groupe(),
        eleve_groupe = get_etu_groupe(),
        etudiants = get_all_etu(),
        #moyenne_classe = get_moy_classe(classe),
        #nb_eleve = get_nb_eleve(classe),
        moyennes_classes = pour_nico(),
        form = CreerGroupeForm()
    )

@app.route("/professeur/escapeCree", methods=['get','post'])
@login_required
def professeurEscape():
    if request.form.get('date') != None:
        date = request.form['date']
        classe = request.form.getlist('groupes')
        ids = request.form['idS']
        publier(date,classe,ids)
    return render_template(
        "professeurEscape.html",
        title = "Professeur création",
        prof = get_prof_nom(),
        escapes = get_scenario_profi(),
        groupe = get_classe()
    )

@app.route("/supprimer",methods=['get','post'])
@login_required
def supprimer_un_scenario():
    escape = request.form.get("escape")
    supprimer_scenario(escape)
    return redirect(url_for("professeurEscape"))

@app.route("/professeur/creeEscape")
@login_required
def professeur_eleve():
    return render_template(
        "professeurCreer.html",
        title = "Professeur éléve"
    )

class ScenarioForm(FlaskForm):
    titre = StringField('Titre', validators=[DataRequired()])
    texteintro = TextAreaField('texte Introductif', validators=[DataRequired()])
    image = FileField('Choisissez une image:')
    selection = SelectField("Matière", choices=[('1','Mathématiques'),('2','Physique / Chimie'),('3', 'Base de donnée'),('4','Web Serveur'),('5','CPOA')])
    time = TimeField("Temps", validators=[DataRequired()])

@app.route("/scenario", methods=['get','post'])
@login_required
def creation_scenario():
    f = ScenarioForm()
    selec = ['Mathématiques','Physique / Chimie','Base de donnée','Web Serveur','CPOA']
    if f.validate_on_submit():
        image = photos.save(f.image.data)
        new_scenario(f.titre.data,f.texteintro.data,image, selec[int(f.selection.data)],f.time.data,get_prof_nom())
        return redirect('/scenario/salle')
    return render_template(
        "creationScenario.html",
        title = "Creation Scénario",
        form=f
    )

@app.route("/scenario/salle", methods=['get','post'])
@login_required
def creation_salle():
    if request.method == "POST":
        res = list()
        a = get_scenario_salle(request.form['select1'])
        image = photos.save(request.files['img0'])
        salle = new_salle(request.form['titre0'],image)
        construire(a,salle)
        res.append(salle)
        update_scenario(a,salle)
        for i in range(int(request.form['coucou'])-1):
            image = photos.save(request.files['img'+str(i+1)])
            sallebis = new_salle(request.form['titre'+str(i+1)],image)
            construire(a,sallebis)
            res.append(sallebis)
        suivre(res)
        return redirect('/scenario/enigme')
    return render_template(
        "creationSalle.html",
        title = "Création salle",
        choices=get_scenario_prof_nom(current_user.mail)
        )

class EnigmeForm(FlaskForm):
    # scenario = SelectField("Choisir un scénario",validators=[DataRequired()],choices=get_scenario_prof_nom(current_user))
    # salle = SelectField("Choisir une salle",validators=[DataRequired()],choices =[])
    file = FileField("Image",validators=[DataRequired()])
    titre = StringField('Titre',validators=[DataRequired()])
    domaine = StringField('Domaine de la matière',validators=[DataRequired()])
    texte = TextAreaField("Texte de l'énigme",validators=[DataRequired()])
    reponse = StringField('Réponse',validators=[DataRequired()])
    indice = StringField("Indice",validators=[DataRequired()])
    points = IntegerField("Points de l'énigme",validators=[DataRequired(),NumberRange(1,10)])



@app.route("/scenario/enigme",methods=("GET","POST"))
@login_required
def creation_enigme():
    '''page de creation d enigme'''
    choice = get_scenario_prof_nom(current_user.mail)
    f = EnigmeForm()
    print("wtf")
    if request.method == "POST":
        print("je suis passer")
        titre = f.titre.data
        domaine = f.domaine.data
        texte = f.texte.data
        reponse = f.reponse.data
        points = f.points.data
        indice = f.indice.data
        img = photos.save(f.file.data)
        ajout_enigme(titre,texte,reponse,domaine,points,indice,"easy",request.form['select2'],scenar_salle2(current_user.mail),request.form['select1'],img)
    return render_template("creationEnigme.html",form=f,
                           dict_senar_salle = scenar_salle(current_user.mail),
                           choices=get_scenario_prof_nom(current_user.mail),
                           my_json = json.dumps(scenar_salle(current_user.mail)))

@app.route("/scenario/ajouterEnigme", methods=("GET","POST"))
@login_required
def placer_enigme():
    if request.form.get("select1") != None:
        ee = get_all_enigme_aa()
        x = request.form["select1"]
        e = str(ee[int(x)].id)
        return redirect("/scenario/ajouterEnigme/salle/"+x+"/"+e)
    return render_template(
        "placerEnigme.html",
        title = "Placer Enigme",
        prof = get_prof_nom(),
        enigme = get_all_enigme_aa(),
        salle = get_salle_enigme(),
    )

@app.route("/scenario/ajouterEnigme/salle/<int:ids>/<int:ide>", methods=("GET","POST"))
def placer_salle(ids,ide):
    if request.form.getlist('posx') != []:
        x = request.form.getlist('posx')
        y = request.form.getlist('posy')
        salle = request.form.getlist('id_salle')
        enigme = request.form.getlist('id_enigme')
        placer(x[0],y[0],salle[0],enigme[0])
        return redirect("/scenario/ajouterEnigme")
    return render_template(
        "placerEnigmeSalle.html",
        id_enigme = ids,
        iden = ide,
        salle = get_salle_enigme(),
    )

@app.route("/noter", methods=("POST",))
@login_required
def noter():
    heure = int(request.form.get("heureRestante"))
    minute = int(request.form.get("minuteRestante"))
    seconde = int(request.form.get("secondeRestante"))
    partie = get_partie(request.form.get("id_partie"))
    mise_a_jour_temps(partie.id,heure,minute,0)
    note = request.form.get("note")
    set_note(partie.id,partie.groupe_id,note)
    return redirect(url_for("escapeGame"))


class LoginForm(FlaskForm):
    mail = StringField('Mail', validators=[DataRequired()])
    motDePasse = PasswordField('motDePasse', validators=[DataRequired()])

    def get_authenticated_user(self):
        user = Utilisateur.query.get(self.mail.data)
        if user is None:
            return None
        m = sha256()
        m.update(self.motDePasse.data.encode())
        passwd = m.hexdigest()
        return user if passwd == user.motDePasse else None

@app.route ("/login/", methods =("GET","POST",))
def login():
    f = LoginForm()
    if f.validate_on_submit():
        user = f.get_authenticated_user()
        if user:
            login_user(user)
            try:
                get_prof_nom(user.mail)
                return redirect(url_for("professeurEleve"))
            except:
                try:
                    get_admin_nom(user.mail)
                    return redirect(url_for("BasesDonnees"))
                except:
                    return redirect(url_for("escapeGame"))
        else:
            return render_template(
                "login.html",
                form=f,
                error=True,
                moyenne = moyenne_des_votes())
    return render_template(
        "login.html",
        form=f,
        moyenne = moyenne_des_votes())

@app.route("/logout/")
@login_required
def logout():
    logout_user()
    return redirect(url_for('home'))

@app.route("/creergroupe", methods =("POST",))
@login_required
def creer_groupe():
    creer_groupes(request.form['nom'])
    return redirect(url_for("professeurEleve"))

@app.route("/suppgroupe", methods =("POST",))
@login_required
def supp_groupe():
    supp_groupes(request.form['groupe'])
    return redirect(url_for("professeurEleve"))

@app.route("/ajout_eleve_groupe", methods =("POST",))
@login_required
def ajout_eleve_groupe():
    idGroupe = int(request.form['groupe'])
    if idGroupe > 0:
        checkbox = request.form.getlist('checkboxetu')
        for etu in checkbox:
            ajout_etu_groupe(etu, idGroupe)
    return redirect(url_for("professeurEleve"))

class parametreform(FlaskForm):
    file = FileField("Changez votre image de profil:")
    nom = StringField('Changez votre nom:',validators=[DataRequired()])
    prenom = StringField('Changez votre prenom:',validators=[DataRequired()])
    mail = StringField('Changez votre address Mail:',validators=[DataRequired()])
    motDePasse = PasswordField("Changez votre mot de passe:", validators=[DataRequired()])

@app.route("/parametre", methods=("POST","GET"))
@login_required
def parametre():
    f = parametreform()
    if f.validate_on_submit():
        m = sha256()
        m.update(f.motDePasse.data.encode())
        image = photos.save(f.file.data)
        update_info_utilisateur(f.mail.data,f.nom.data,f.prenom.data,m.hexdigest(),image)
        return redirect("/retour")
    return render_template(
        "parametre.html",
        form = f,
    )

@app.route("/professeur/statEleve/<string:mail>")
def statEleve(mail):
    return render_template(
        "statEleve.html",
        prof = get_prof_nom(),
        eleve = get_Etu(mail),
        moyenne = moyenne_total(mail),
        realise = get_note_scena2(mail),
    )

@app.route("/admin/statistics")
@login_required
def stats():
	nombreprof = nb_prof()
	nombreEleve = nb_eleve()
	nombreEscape = nb_escape_creer()
	return render_template("statistics.html", title= "administrateur",nb_prof = nombreprof, nb_eleve = nombreEleve,nb_escape = nombreEscape)

class NvProfForm(FlaskForm):
	NomNvProf = StringField('Nom',validators=[DataRequired()])
	PrenomNvProf = StringField('Prenom',validators=[DataRequired()])
	MailNvProf = StringField("Mail",validators=[DataRequired()])
	MDPTemp = StringField('Mot de Passe',validators=[DataRequired()])
	ImageProf = StringField("Image",validators=[DataRequired()])
class SPProfForm(FlaskForm):
	MailSPProf = StringField("Mail",validators=[DataRequired()])


@app.route("/admin/CreerProf", methods = ("POST","GET"))
@login_required
def CreerProf():
	a = NvProfForm()
	if a.validate_on_submit():
		NomNvProf = a.NomNvProf.data
		PrenomNvProf = a.PrenomNvProf.data
		MailNvProf = a.MailNvProf.data
		MDPTemp = a.MDPTemp.data
		ImageProf = a.ImageProf.data
		m=sha256()
		m.update(MDPTemp.encode())
		try:
			test=Utilisateur.query.filter(Utilisateur.mail == MailNvProf).one()
			testprof=Professeur(mail = MailNvProf, utilisateur = MailNvProf)
			db.session.add(testprof)
		except:
			test=Utilisateur(mail = MailNvProf,nom = NomNvProf, prenom = PrenomNvProf,motDePasse = m.hexdigest(),imageProfil = ImageProf)
			db.session.add(test)
			testprof=Professeur(mail = MailNvProf, utilisateur = test	)
			db.session.add(testprof)
			db.session.commit()
	b = SPProfForm()
	if b.validate_on_submit():
		MailSPProf = b.MailSPProf.data
		try:
			testprofS= Professeur.query.filter(Professeur.mail == MailSPProf).one()
			db.session.delete(testprofS)
		except:
			pass
		db.session.commit()
	return render_template("CreerProf.html", title= "administrateur", form = a,form2 = b)



@app.route("/admin/BasesDonnées")
@login_required
def BasesDonnees():
	professeurs = get_liste_prof()
	eleves = get_liste_eleve()
	return render_template("BaseDonneeAdmin.html", title = "Base Donnée", prof = professeurs, eleves = eleves)


class NvEtuForm(FlaskForm):
    NomNvEtu = StringField('Nom',validators=[DataRequired()])
    PrenomNvEtu = StringField('Prenom',validators=[DataRequired()])
    MailNvEtu = StringField("Mail",validators=[DataRequired()])
    MDPTemp = StringField('Mot de passe',validators=[DataRequired()])
    ImageEtu = FileField("Image",validators=[DataRequired()])
    EtabliNvEtu = StringField('Etablissement',validators=[DataRequired()])
    PromoNvEtu = StringField('Promotion',validators=[DataRequired()])
    ClasseNvEtu = StringField('Classe',validators=[DataRequired()])
    GroupeNvEtu = StringField('Groupe',validators=[DataRequired()])
class SPEtuForm(FlaskForm):
    MailSPEtu = StringField("Mail",validators=[DataRequired()])

@app.route("/admin/gestionEtu", methods = ("POST","GET"))
@login_required
def GestionEtu():
    a = NvEtuForm()
    if a.validate_on_submit():
        NomNvEtu = a.NomNvEtu.data
        PrenomNvEtu = a.PrenomNvEtu.data
        MailNvEtu = a.MailNvEtu.data
        MDPTemp = a.MDPTemp.data
        ImageEtu = a.ImageEtu.data
        EtabliNvEtu = a.EtabliNvEtu.data
        PromoNvEtu = a.PromoNvEtu.data
        ClasseNvEtu = a.ClasseNvEtu.data
        GroupeNvEtu = photos.save(a.GroupeNvEtu.data)
        try:
            if Utilisateur.query.filter(Utilisateur.mail == MailNvEtu).one():
                flash("Cette adresse mail est déjà enregistrée")
        except:
            test=Utilisateur(mail = MailNvEtu,nom = NomNvEtu, prenom = PrenomNvEtu,motDePasse = MDPTemp,imageProfil = ImageEtu)
            db.session.add(test)
            testpromo=Promotion(nom = PromoNvEtu, etablissement = EtabliNvEtu)
            db.session.add(testpromo)
            testclasse=Classe(nom = ClasseNvEtu, promotion = testpromo)
            db.session.add(testclasse)
            testgroupe=Groupe(nom = GroupeNvEtu)
            db.session.add(testgroupe)
            testetu=Etudiant(mail = MailNvEtu, promotion = testpromo, classe = testclasse, utilisateur = test	)
            db.session.add(testetu)
        db.session.commit()
    b = SPEtuForm()
    if b.validate_on_submit():
        MailSPEtu = b.MailSPEtu.data
        testetuS= Etudiant.query.filter(Etudiant.mail == MailSPEtu).one()
        db.session.delete(testetuS)
        db.session.commit()
    return render_template("gestionEtu.html", title= "administrateur", form = a,form2 = b)

@app.route("/voter", methods = ("POST",))
@login_required
def voter():
    etoile = float(request.form.get("note"))
    commentaire = request.form.get("commentaire")
    vote(etoile,commentaire,current_user.mail)
    return redirect(url_for("parametre"))

@app.route("/retour")
@login_required
def retour():
    try:
        get_prof_nom(current_user.mail)
        return redirect(url_for("professeurEleve"))
    except:
        try:
            get_admin_nom(current_user.mail)
            return redirect(url_for("BasesDonnees"))
        except:
            return redirect(url_for("escapeGame"))
