from ctypes import sizeof
from mmap import ACCESS_COPY
from .models import *
from .modelsEtu import *
from datetime import datetime
from hashlib import sha256

def get_prof_nom(mail="julesvelluet@gmail.com"):
    return Professeur.query.filter(Professeur.mail == mail).all()[0]

def get_scenario_prof_nom(emailP):
    res = []
    a = Scenario.query.filter(Scenario.prof_mail == emailP).all()
    for b in a:
        res.append(b.nom)
    return res

def get_scenario_profi(emailP="julesvelluet@gmail.com"):
    return Scenario.query.filter(Scenario.prof_mail == emailP).all()

def get_classe():
    return Classe.query.all()

def get_etu_classe():
    res = dict()
    classes = get_classe()
    for classe in classes:
            etu = get_etu_c(classe.id)
            res[classe] = etu
    return res

def get_etu_c(id):
    return Etudiant.query.filter(Etudiant.classe_id == id).all()

def moy_eleve(eleve):
    res=0
    for obten in get_all_obtention(eleve):
        res+=obten.note
    if len(get_all_obtention(eleve)) == 0:
        return 0
    return res/len(get_all_obtention(eleve))

def get_moy_classe(classe):
    res = 0
    cpt = 0
    etudiant = get_etu_c(classe.id)
    for i in range(len(etudiant)):
        res+=moy_eleve(etudiant[i])
    for etu in etudiant:
        if len(get_all_obtention(etu)) == 0:
            cpt +=1
    if len(etudiant)-cpt <= 0:
        return "Pas de note"
    return res/(len(etudiant)-cpt)

def get_all_obtention(eleve):
    return Obtention.query.filter(Obtention.etudiant_mail==eleve.mail).all()

def pour_nico():
    classes = Classe.query.all()
    dico = {}
    for classe in classes:
        dico[classe] = [get_nb_eleve(classe), get_moy_classe(classe)]
    return dico


def get_nb_eleve(classe):
    res = 0
    for i in range(len(Etudiant.query.filter(Etudiant.classe_id==classe.id).all())):
        res+=1
        i+=1
    return res

def new_classe(nomClasse, promo):
    pro = get_id_promo(promo)
    db.session.add(Classe(nom=nomClasse,promo_id=pro.id))
    db.session.commit()

def new_eleve(nomE,prenomE,emailE,mdpE,promotion,classe):
    m = sha256()
    m.update(mdpE.encode())
    db.session.add(Utilisateur(nom=nomE,prenom=prenomE,mail=emailE,motDePasse=m.hexdigest(),imageProfil="https://www.zupimages.net/up/21/43/3kq9.png"))
    db.session.commit()
    idU = Utilisateur.query.filter(Utilisateur.mail == emailE).all()[0]
    try:
        promo = get_id_promo(promotion)
    except:
        db.session.add(Promotion(nom=promotion,etablissement="iut"))
        db.session.commit()
        promo = get_id_promo(promotion)
    try:
        cla = get_id_classe(promo,classe)
    except:
        db.session.add(Classe(nom=classe,promo_id=promo.id))
        db.session.commit()
        cla = get_id_classe(promo,classe)
    db.session.add(Etudiant(mail=idU.mail,promo_id=promo.id,classe_id=cla.id))
    db.session.commit()

def get_id_promo(promo):
    return Promotion.query.filter(Promotion.nom == promo).all()[0]

def get_id_classe(promo,classe):
    return Classe.query.filter(Classe.nom == classe and Classe.promo_id == promo.id).all()[0]

def ajout_eleve_classe(liste_eleve):
    for dict in liste_eleve:
        for status,eleve in dict.items():
            if status == "Classe":
                classe = eleve
            elif status == "Promotion":
                promo = eleve
            elif status == "Mail":
                mail = eleve
            elif status == "Mot de Passe":
                mdp = eleve
            elif status == "Nom":
                nom = eleve
            elif status == "Prenom":
                prenom = eleve
        new_eleve(nom,prenom,mail,mdp,promo,classe)

def supp_eleve(email):
    user = Utilisateur.query.filter(Utilisateur.mail == email).all()[0]
    idU = Etudiant.query.filter(Etudiant.mail == user.mail).all()[0]
    ap = Appartenir.query.filter(Appartenir.etudiant_mail == idU.mail).all()
    for a in ap:
        db.session.delete(a)
    ob = Obtention.query.filter(Obtention.etudiant_mail == idU.mail).all()
    for o in ob:
        db.session.delete(o)
    db.session.commit()
    db.session.delete(idU)
    db.session.commit()
    db.session.delete(user)
    db.session.commit()

def add_eleve(classeEleve, nomEleve, mailEleve,promo):
    eleve = Utilisateur.query.filter(Utilisateur.mail == mailEleve).all()[0]
    promo = get_id_promo(promo)
    cla = get_id_classe(promo,classeEleve)
    etu = Etudiant.query.filter(Etudiant.mail == eleve.mail).all()[0]
    db.session.query(Etudiant).filter(Etudiant.mail == eleve.mail).update({Etudiant.promo_id:promo.id,Etudiant.classe_id:cla.id})
    db.session.commit()

def supp_classe(classe):
    clas = Classe.query.filter(Classe.nom == classe).all()[0]
    db.session.delete(clas)
    db.session.commit()

def add_promo(promo):
    db.session.add(Promotion(nom = promo, etablissement = "genial"))
    db.session.commit()

def new_scenario(titre,texte,image,selection,time,prof):
    db.session.add(Scenario(nom=titre,image=image,description=texte,temps=time,domaine=selection,prof_mail=prof.mail))
    db.session.commit()

def new_salle(title,img):
    s = Salle(titre=title,image=img)
    db.session.add(s)
    db.session.commit()
    return s

def get_scenario_salle(id):
    return Scenario.query.filter(Scenario.id == id).all()[0]


def update_scenario(s,salle):
    db.session.query(Scenario).filter(Scenario.id == s.id).update({Scenario.salle_id:salle.id})
    db.session.commit()

def construire(s,salle):
    db.session.add(Construire(salle_id=salle.id,scenario_id=s.id))
    db.session.commit()

def suivre(liste):
    if len(liste) == 1:
        db.session.add(Suivre(salle_actuelle_id=liste[0].id,salle_suivante_id=liste[0].id,salle_precedente_id=liste[0].id))
        db.session.commit()
    elif len(liste) == 2:
        db.session.add(Suivre(salle_actuelle_id=liste[0].id,salle_suivante_id=liste[1].id,salle_precedente_id=liste[1].id))
        db.session.commit()
        db.session.add(Suivre(salle_actuelle_id=liste[1].id,salle_suivante_id=liste[0].id,salle_precedente_id=liste[0].id))
        db.session.commit()
    else:
        for i in range(len(liste)-1):
            db.session.add(Suivre(salle_actuelle_id=liste[i].id,salle_suivante_id=liste[i+1].id,salle_precedente_id=liste[i-1].id))
            db.session.commit()
        a = len(liste)-1
        db.session.add(Suivre(salle_actuelle_id=liste[a].id,salle_suivante_id=liste[0].id,salle_precedente_id=liste[a-1].id))
        db.session.commit()

def get_all_salles2(id):
    res = []
    for i in  Construire.query.filter(Construire.scenario_id == id).all():
        res.append(i.salle.titre)
    return res

def scenar_salle(mail="julesvelluet@gmail.com"):
    res = dict()
    scenar = get_scenario_profi(mail)
    for sc in scenar:
        salle = get_all_salles2(sc.id)
        res[sc.id] = salle
    return res

def scenar_salle2(mail="julesvelluet@gmail.com"):
    res = dict()
    scenar = get_scenario_profi(mail)
    for sc in scenar:
        salle = get_all_salles(sc.id)
        res[sc.id] = salle
    return res

def ajout_enigme(titre,texte,reponse,domaine,points,texte_indice,matiere,salle,dict,sc,img):
    for (keys, value) in dict.items():
        if (int(sc)+1 == int(keys)):
            id_salle = value[int(salle)].id
    # id_salle = db.session.query(Salle).filter(Salle.id==int(salle)).one()
    indice = Indice(texte=texte_indice)
    db.session.add(indice)
    db.session.commit()
    mat = Matiere(intitule=matiere)
    db.session.add(mat)
    db.session.commit()
    enigme = Enigme(titre=titre,texte=texte,reponse=reponse,domaine=domaine,points=points,indice_id=indice.id,matiere_id=mat.id,salle_id=id_salle, image = img)
    print(enigme)
    db.session.add(enigme)
    db.session.commit()

def update_info_utilisateur(mail,nom,prenom,mdp,image):
    db.session.query(Utilisateur).filter(Utilisateur.mail == mail).update({Utilisateur.mail:mail,Utilisateur.nom:nom,Utilisateur.prenom:prenom,Utilisateur.motDePasse:mdp,Utilisateur.imageProfil:image})
    db.session.commit()

def get_all_enigme_aa():
    return db.session.query(Enigme).all()

def get_salle_enigme():
    res = []
    a = get_all_enigme_aa()
    for e in a:
        a = Enigme.query.filter(Enigme.id == e.id).all()[0]
        s = Salle.query.filter(Salle.id == a.salle_id).all()[0]
        res.append(s)
    return res

def placer(x,y,salle,enigme):
    contient = Contient(x=x,y=y,salle_id=salle,enigme_id=enigme)
    db.session.add(contient)
    db.session.commit()

def publier(date,classe,ids):
    dd = ""
    for d in range(len(date)):
        if date[d] != "-":
            dd += date[d]
        else:
            if d == 4:
                years = dd
                dd = ""
            elif d == 7:
                month = dd
                dd = ""
    day = dd
    dada = day +"-"+ month + "-" + years
    da = datetime.strptime(dada, "%d-%m-%Y")
    for id in classe:
        a = Accessible(date=da,classe_id=id,scenario_id=ids)
        db.session.add(a)
        db.session.commit()
def get_enigme_salles(idSalle):
    return Enigme.query.filter(Enigme.salle_id==idSalle).all()

def get_all_avancer(enigme):
    return Avancer.query.filter(Avancer.enigme_id==enigme.id).all()

def get_all_contient(enigme,salle):
    return Contient.query.filter(Contient.salle_id==salle.id and Contient.enigme_id==enigme.id).all()

def get_all_construire(salle,iddscenario):
    return Construire.query.filter(Construire.salle_id==salle.id and Construire.scenario_id==iddscenario).all()

def get_all_suivre(salle):
    return Suivre.query.filter(Suivre.salle_actuelle_id==salle.id).all()

def get_all_accessible(idScena):
    return Accessible.query.filter(Accessible.scenario_id==idScena).all()

def supprimer_scenario(idSce):
    for salle in get_all_salles(idSce):
        for enigme in get_enigme_salles(salle.id):
            db.session.delete(Indice.query.filter(Indice.id==enigme.indice_id).one())
            db.session.commit()
            for avance in get_all_avancer(enigme):
                db.session.delete(avance)
                db.session.commit()
            for contient in get_all_contient(enigme,salle):
                db.session.delete(contient)
                db.session.commit()
            for cons in get_all_construire(salle,idSce):
                db.session.delete(cons)
                db.session.commit()
            db.session.delete(Enigme.query.filter(Enigme.id==enigme.id).one())
            db.session.commit()
        for suiv in get_all_suivre(salle):
            db.session.delete(suiv)
            db.session.commit()
        db.session.delete(Salle.query.filter(Salle.id==salle.id).one())
        db.session.commit()
        for acce in get_all_accessible(idSce):
            db.session.delete(acce)
            db.session.commit()
    db.session.delete(Scenario.query.filter(Scenario.id==idSce).one())
    db.session.commit()
