from flask import Flask
from flask_login import LoginManager

app = Flask(__name__)

import os.path

def mkpath(p):
    return os.path.normpath (
        os.path.join(
            os.path.dirname(__file__ ),
            p))

from flask_sqlalchemy import SQLAlchemy
app.config['SQLALCHEMY_DATABASE_URI'] = ('sqlite:///'+mkpath('../myapp.db'))
db = SQLAlchemy(app)

app.config['SECRET_KEY'] = "da216cdc-114c-4914-9775-34b5195cf66b"
app.config["UPLOADED_PHOTOS_DEST"] = "application/static/img"

login_manager = LoginManager(app)
login_manager.login_view = "login"
