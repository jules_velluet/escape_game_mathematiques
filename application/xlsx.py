from openpyxl import Workbook, load_workbook
import copy
from .modelsProf import ajout_eleve_classe

def load_xlsx(exel):
    etu = load_workbook('application/static/img/'+exel)
    etua = etu.active
    a = dict_etu(etua)
    ajout_eleve_classe(a)

def dict_etu(etua):
    res = []
    dico = {"Nom":None,"Prenom":None,"Classe":None,"Promotion":None,"Mail":None,"Mot de Passe":None}
    for row in etua.values:
        i = 0
        dic = copy.deepcopy(dico)
        for value in row:
            if i == 0:
                dic['Nom'] = value
            elif i == 1:
                dic['Prenom'] = value
            elif i == 2:
                dic['Classe'] = value
            elif i == 3:
                dic['Promotion'] = value
            elif i == 4:
                dic['Mail'] = value
            elif i == 5:
                dic['Mot de Passe'] = value
            i +=1
        res.append(dic)
    res.pop(0)
    return res

# def decoupe(dic):
#     cpt = 0
#     thread = []
#     coeur = multiprocessing.cpu_count()
#     nb_coueur = len(dic)//coeur
#     print(len(dic))
#     for i in range(0,coeur):
#         if i < nb_coueur+1:
#             t = threading.Thread(target=ajout_eleve_classe2, args=(dic,i*nb_coueur,(i+1)*nb_coueur))
#             t.start()
#             thread.append(t)
#             cpt +=1
#         else:
#             t = threading.Thread(target=ajout_eleve_classe2, args=(dic,nb_coueur*i,len(dic)))
#             t.start()
#             thread.append(t)
#             cpt +=1
#     for t in thread:
#         t.join()
