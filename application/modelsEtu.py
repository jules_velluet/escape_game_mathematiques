from .models import *

def get_Etu(mail):
    return Etudiant.query.filter(Etudiant.mail == mail).all()[0]

def get_user(mail):
    return Utilisateur.query.filter(Utilisateur.mail == mail).all()[0]

def moyenne_total(mail):
    dico = get_note_scena(mail)
    total = 0
    nb_escape = 0
    for note in dico.values():
        if note[0] != "Pas de note":
            total += note[0]
            nb_escape += 1
    if nb_escape ==0:
        return total
    return total/nb_escape


def get_groupe(mail):
    return Appartenir.query.filter(Appartenir.etudiant_mail == mail).all()

def creer_groupes(n):
    o = Groupe(nom=n)
    db.session.add(o)
    db.session.commit()
    return o.id

def supp_groupes(n):
    try:
        o = Groupe.query.filter(Groupe.id == n).one()
        appartenir = o.appartenirs
        for a in appartenir:
            db.session.delete(a)
        db.session.delete(o)
        db.session.commit()
    except:
        pass


def ajoute_eleve_groupe(mail,groupe_id):
    o = Appartenir(groupe_id = groupe_id, etudiant_mail = mail)
    db.session.add(o)
    db.session.commit()

def get_etu_groupe():
    res = dict()
    for etu in Etudiant.query.all():
        for app in etu.appartenirs:
            if app.groupe in res:
                res[app.groupe].append(etu)
            else:
                res[app.groupe] = [etu]
    for groupe in Groupe.query.all():
        if groupe not in res:
            res[groupe] = []
    return res

def get_etu_id_groupe(mail):
    res = []
    app = Appartenir.query.filter(Appartenir.etudiant_mail == mail)
    for a in app:
        res.append(a.groupe_id)
    return res

def get_all_etu():
    return Etudiant.query.all()

def ajout_etu_groupe(etu, idGroupe):
    try:
        o = Appartenir(groupe_id=idGroupe, etudiant_mail=etu)
        db.session.add(o)
        db.session.commit()
    except:
        db.session.rollback()

def get_all_groupe():
    return Groupe.query.all()

# Partie

def get_parties_realises(mail):
    res = []
    groupe = []
    for x in Appartenir.query.filter(Appartenir.etudiant_mail == mail).all():
        groupe.append(x.groupe)
    for i in Partie.query.all():
        if i.groupe in groupe:
            res.append(i)
    return res

def newPartie(etudiant_id, heure, minute, seconde, escape_id, groupe):
    o = Partie(finit = False, scenario_id = escape_id, groupe_id = groupe, timer = time(heure, minute, seconde))
    db.session.add(o)
    db.session.commit()
    return o

def get_partie(id):
    return Partie.query.filter(Partie.id == id).one()

def mise_a_jour_temps(id,heure,minute,seconde):
    partie = get_partie(id)
    partie.timer = time(heure,minute,seconde)

def get_note(idPartie, mailEtudiant):
    try:
        return Obtention.query.filter(Obtention.partie_id == idPartie).filter(Obtention.etudiant_mail == mailEtudiant).one().note
    except:
        return "Pas de note"

def set_note(idPartie, id_groupe, note):
    appartenir = Appartenir.query.filter(Appartenir.groupe_id == id_groupe).all()
    for app in appartenir:
        try:
            o = Obtention(partie_id = idPartie, etudiant_mail = app.etudiant_mail, note = note)
            db.session.add(o)
            db.session.commit()
        except:
            db.session.rollback()
            Obtention.query.filter(Obtention.partie_id == idPartie).filter(Obtention.etudiant_mail == app.etudiant_mail).note = note
            db.session.commit()



# Scenario

def get_all_scenario():
    return Scenario.query.all()

def get_scenario_accessible(mail):
    res = []
    for x in Accessible.query.all():
        if Etudiant.query.filter(Etudiant.mail == mail).all()[0].classe_id == x.classe_id and x.date <= date.today():
            res.append(x.scenario)
    return res

def get_durer(id):
    return Scenario.query.filter(Scenario.id ==id).all()[0].temps

def get_introduction(id):
    return Scenario.query.filter(Scenario.id ==id).all()[0].description

def get_scenario(id):
    return Scenario.query.filter(Scenario.id == id).all()[0]

def get_scenario_prof(id):
    return Scenario.query.filter(Scenario.prof_id == id).all()

def get_scennario_realiser(mail):
    scena = []
    for partie in get_parties_realises(mail):
        scenario = partie.scenario
        if scenario not in scena:
            scena.append(scenario)
    return scena


def get_note_one_scena(mail,idScenario):
    res = []
    cpt = 0
    abandon = 0
    note = []
    groupes = get_etu_id_groupe(mail)
    for idGroupe in groupes:
        all_partie = Partie.query.filter(Partie.scenario_id == idScenario).filter(Partie.groupe_id == idGroupe)
        for partie in all_partie:
            cpt += 1
            obtentions = partie.obtentions.filter(Obtention.etudiant_mail==mail).all()
            if obtentions == []:
                abandon +=1
            else:
                note.append(obtentions[0].note)
    if len(note) == 0:
        res.append("Pas de note")
    else:
        res.append(round(sum(note)/len(note),2))
    res.append(cpt)
    res.append(abandon)
    return res

def get_note_scena(mail):
    dico = {}
    for escape in get_scennario_realiser(mail):
        dico[escape.id] = get_note_one_scena(mail,escape.id)
    return dico

def get_note_scena2(mail):
    dico = {}
    for escape in get_all_scenario():
        try:
            cpt = 0
            abandon = 0
            all_partie = Partie.query.filter(Partie.scenario_id == escape.id).all()
            all_obtension = []
            for partie in all_partie:
                all_obtension.append(partie.obtentions.filter(Obtention.etudiant_mail==mail).all())
                exits = partie.obtentions.filter(Obtention.etudiant_mail==mail).scalar()
                if exits == None:
                    abandon += 1
                    cpt += 1
            liste = []
            for ob in all_obtension:
                for o in ob:
                    cpt += 1
                    liste.append(o.note)
            dico[escape.nom] = [round(sum(liste)/len(liste),2), cpt, abandon]
        except:
            dico[escape.nom] = ["Pas de note",0, abandon]
    return dico
# Salle

def get_user(mail):
    return Utilisateur.query.filter(Utilisateur.mail == mail).all()[0]


def get_all_salles(id):
    res = []
    for i in  Construire.query.filter(Construire.scenario_id == id).all():
        res.append(i.salle)
    return res

def get_all_salles_nom(name):
    for i in Construire.query.all():
        if i.name==name:
            id=i.scenario_id
    return get_all_salles(id)

def get_all_salles_nom_bis(name):
    name = []
    for i in Scenario.query.filter(Scenario.titre == name).all():
        name.append(i.titre)
    return name

def get_direction(id):
    res = {}
    for salle in get_all_salles(id):
        query = Suivre.query.filter(Suivre.salle_actuelle_id == salle.id)[0]
        res[salle.id] = (query.salle_actuelle ,query.salle_precedente, query.salle_suivante)
    return res

# Enigme

def get_enigme_salle(id):
    return Contient.query.filter(Contient.salle_id == id).all()

def get_enigme_salles(idSalle):
    return Enigme.query.filter(Enigme.salle_id==idSalle).all()

def get_enigme(id):
    return Enigme.query.filter(Enigme.id == id).all()[0]

def get_reponse_donnee(idEnigme,idPartie):
    try:
        return Avancer.query.filter(Avancer.enigme_id == idEnigme).filter(Avancer.partie_id == idPartie).one().reponseDonnee
    except:
        return "Aucune réponse n'a encore été donnée"

def set_reponse_donnee(idEnigme,idPartie, reponse):
    try:
        Avancer.query.filter(Avancer.enigme_id == idEnigme).filter(Avancer.partie_id == idPartie).one().reponseDonnee = reponse
    except:
        o = Avancer(resolue = False, reponseDonnee = reponse, enigme_id = idEnigme, partie_id = idPartie)
        db.session.add(o)
    db.session.commit()

def get_enigmes_scena_salle(idScena,idSalle):
    res=[]
    salles = get_all_salles(idScena)
    for salle in salles :
        if salle.id == idSalle:
            res=get_enigme_salles(salle.id)
    return res

def get_enigmes_scenario(id):
    res = []
    salles = get_all_salles(id)
    for salle in salles:
        res.extend(get_enigme_salle(salle.id))
    return res

def get_all_enigme(sce):
    return Enigme.query.filtre(Scenario.id == sce).all()

def get_points_enigme(idEnigme):
    return Enigme.query.filter(Enigme.id == idEnigme).one().points

def incremente_essaie(enigmeId,partieId):
    Avancer.query.filter(Avancer.enigme_id == enigmeId).filter(Avancer.partie_id == partieId).one().essaie += 1
    db.session.commit()

def get_essaie(enigmeId, partieId):
    try:
        return Avancer.query.filter(Avancer.enigme_id == enigmeId).filter(Avancer.partie_id == partieId).one().essaie
    except:
        o = Avancer(enigme_id = enigmeId, partie_id = partieId)
        db.session.add(o)
        db.session.commit()
        return o.essaie
