import gspread
from oauth2client.service_account import ServiceAccountCredentials
import json, os.path



def recup():
    '''
    recupére les donnée d'un google sheets sous la forme d'une liste de dictionaire
    '''
    # with open('/mnt/g/projet/escape_game_mathematiques/application/creds.json') as json_data:
    #     data_dict = json.load(json_data)
    #     print(data_dict)
    scope = ["https://spreadsheets.google.com/feeds",'https://www.googleapis.com/auth/spreadsheets',"https://www.googleapis.com/auth/drive.file","https://www.googleapis.com/auth/drive"]
    creds = ServiceAccountCredentials.from_json_keyfile_name("application/static/creds.json", scope)

    client = gspread.authorize(creds)
    sheet = client.open("Renseignements_des_étudiants").sheet1

    data = sheet.get_all_records()
    return data
