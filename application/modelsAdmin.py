from .models import *

def get_admin_nom(mail):
    return Administrateur.query.filter(Administrateur.mail == mail).all()[0]

def get_liste_prof():
	return Professeur.query.all()

def nb_prof():
	res = 0
	for i in get_liste_prof():
		res = res + 1
	return res

def get_liste_eleve():
	return Etudiant.query.all()

def nb_eleve():
	res = 0
	for i in get_liste_eleve():
		res = res + 1
	return res

def get_liste_escape_creer():
    return Scenario.query.all()

def nb_escape_creer():
    res = 0
    for i in get_liste_escape_creer():
        res += 1
    return res
