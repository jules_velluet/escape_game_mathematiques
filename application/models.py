# -*- coding: utf-8 -*-

import yaml, os.path
from .app import db
from datetime import date
from datetime import time
from sqlalchemy.sql import func
from flask_login import UserMixin
from .app import login_manager


class Indice (db.Model):
    id = db.Column(db.Integer, primary_key=True)
    texte = db.Column(db.String(400))

    def __repr__(self):
        return "<Indice " + str(self.id) + ": " + self.texte

class Promotion (db.Model):
    id = db.Column(db.Integer, primary_key=True)
    nom = db.Column(db.String(100))
    etablissement = db.Column(db.String(100))


    def __repr__(self):
        return "<Promotion "  + str(self.id) + " " + self.nom + " " + self.etablissement + ">"

class Utilisateur (db.Model, UserMixin):
    mail = db.Column(db.String(200), primary_key=True)
    nom = db.Column(db.String(200))
    prenom = db.Column(db.String(200))
    motDePasse = db.Column(db.String(200))
    imageProfil = db.Column(db.String(400))

    def get_id(self):
        return self.mail

    def __repr__(self):
        return "<Utilisateur " + self.mail + " " + self.motDePasse

class Groupe (db.Model):
    id = db.Column(db.Integer, primary_key=True)
    nom = db.Column(db.String(200))

    def __repr__(self):
        return self.nom

class Matiere (db.Model):
    id = db.Column(db.Integer, primary_key=True)
    intitule = db.Column(db.String(200))

    def __repr__(self):
        return "<Matiere " + str(self.id) + " : " + self.intitule

class Salle (db.Model):
    id = db.Column(db.Integer, primary_key=True)
    titre = db.Column(db.String(200))
    image = db.Column(db.String(400))

    def __repr__(self):
        return "Salle " + str(self.id) + " : " + self.titre

class Administrateur (db.Model):
    mail = db.Column(db.String(200), db.ForeignKey("utilisateur.mail"), primary_key=True)

    utilisateur = db.relationship("Utilisateur",backref=db.backref("administrateurs",lazy="dynamic"))

    def __repr__(self):
        return "<Administrateur " + str(self.id)


class Professeur (db.Model,):
    mail = db.Column(db.String(200), db.ForeignKey("utilisateur.mail"), primary_key=True)

    utilisateur = db.relationship("Utilisateur",backref=db.backref("prefesseurs",lazy="dynamic"))

    def __repr__(self):
        return "<Professeur " + str(self.mail)

class Scenario (db.Model):
    id = db.Column(db.Integer, primary_key=True)
    nom = db.Column(db.String(200))
    image = db.Column(db.String(400))
    description = db.Column(db.String(200))
    temps = db.Column(db.Time())
    domaine = db.Column(db.String(200))

    salle_id = db.Column(db.Integer, db.ForeignKey("salle.id"))
    salle = db.relationship("Salle",backref=db.backref("scenarios",lazy="dynamic"))

    prof_mail = db.Column(db.String(200),db.ForeignKey("professeur.mail"))
    professeur = db.relationship("Professeur",backref=db.backref("scenarios",lazy="dynamic"))

    matiere_id = db.Column(db.Integer,db.ForeignKey("matiere.id"))
    matiere = db.relationship("Matiere",backref=db.backref("",lazy="dynamic"))

    def __repr__(self):
        return "<Scenario " + str(self.id) + " : " + self.nom + " " + self.description + " " + self.domaine + " " + str(self.matiere_id) + " " + str(self.prof_mail) + " Temps de : " + str(self.temps)

class Construire (db.Model):

    salle_id = db.Column(db.Integer,db.ForeignKey("salle.id"), primary_key = True)
    scenario_id = db.Column(db.Integer,db.ForeignKey("scenario.id"), primary_key = True)

    scenario = db.relationship("Scenario",backref=db.backref("",lazy="dynamic"))
    salle = db.relationship("Salle",backref=db.backref("",lazy="dynamic"))

class Suivre(db.Model):
    salle_actuelle_id = db.Column(db.Integer,db.ForeignKey("salle.id"), primary_key = True)
    salle_suivante_id = db.Column(db.Integer,db.ForeignKey("salle.id"))
    salle_precedente_id = db.Column(db.Integer,db.ForeignKey("salle.id"))

    salle_actuelle = db.relationship("Salle",foreign_keys = salle_actuelle_id,backref=db.backref("suivres",lazy="dynamic"))
    salle_suivante = db.relationship("Salle",foreign_keys = salle_suivante_id,backref=db.backref("suivress",lazy="dynamic"))
    salle_precedente = db.relationship("Salle",foreign_keys = salle_precedente_id,backref=db.backref("suivresss",lazy="dynamic"))

class Enigme (db.Model):
    id = db.Column(db.Integer, primary_key=True)
    titre = db.Column(db.String(200))
    texte = db.Column(db.String(400))
    image = db.Column(db.String(100))
    reponse = db.Column(db.String(100))
    domaine = db.Column(db.String(100))
    points = db.Column(db.Integer)

    indice_id = db.Column(db.Integer,db.ForeignKey("indice.id"))
    matiere_id = db.Column(db.Integer,db.ForeignKey("matiere.id"))
    salle_id = db.Column(db.Integer,db.ForeignKey("salle.id"))

    matiere = db.relationship("Matiere",backref=db.backref("enigmes",lazy="dynamic"))
    indice = db.relationship("Indice",backref=db.backref("enigmes",lazy="dynamic"))
    salle = db.relationship("Salle",backref=db.backref("enigmes",lazy="dynamic"))

    def __repr__(self):
        return "<Enigme "   + " : " + self.titre + " " + self.texte + " " + str(self.matiere_id) + " " + str(self.indice_id) + " " + str(self.salle_id)


class Partie (db.Model):
    id = db.Column(db.Integer, primary_key=True)
    timer = db.Column(db.Time())
    finit = db.Column(db.Boolean)

    scenario_id = db.Column(db.Integer, db.ForeignKey("scenario.id"))
    groupe_id = db.Column(db.Integer, db.ForeignKey("groupe.id"))

    scenario = db.relationship("Scenario",backref=db.backref("parties",lazy="dynamic"))
    groupe = db.relationship("Groupe",backref=db.backref("parties",lazy="dynamic"))

    def __repr__(self):
        return "<Partie " + str(self.id) + " " + str(self.finit) + " " + str(self.scenario_id) + " " + str(self.groupe_id) + " temps de : " + str(self.timer)

class Proposition (db.Model):
    texte = db.Column(db.String(400),primary_key = True)

    enigme_id = db.Column(db.Integer,db.ForeignKey("enigme.id"))

    enigme = db.relationship("Enigme",backref=db.backref("propositions",lazy="dynamic"))

    def __repr__(self):
        return "<Proposition enigme " + str(self.enigme_id) + " : " + self.texte

class Contient (db.Model):
    x = db.Column(db.Float)
    y = db.Column(db.Float)

    salle_id = db.Column(db.Integer, db.ForeignKey("salle.id"), primary_key = True)
    enigme_id = db.Column(db.Integer, db.ForeignKey("enigme.id"), primary_key = True)

    enigme = db.relationship("Enigme",backref=db.backref("contients",lazy="dynamic"))
    salle = db.relationship("Salle",backref=db.backref("contients",lazy="dynamic"))

    def __repr__(self):
        return "<Contient " + str(self.enigme_id) + " " + str(self.salle_id) + " : x=" + str(self.x) + " y=" + str(self.y)

class Avancer (db.Model):
    resolue = db.Column(db.Boolean)
    reponseDonnee = db.Column(db.String(200))
    essaie = db.Column(db.Integer, default = 0)

    enigme_id = db.Column(db.Integer,db.ForeignKey("enigme.id"), primary_key = True)
    partie_id = db.Column(db.Integer,db.ForeignKey("partie.id"), primary_key = True)

    enigme = db.relationship("Enigme",backref=db.backref("avancers",lazy="dynamic"))
    partie = db.relationship("Partie",backref=db.backref("avancers",lazy="dynamic"))

    def __repr__(self):
        return "<Avancer " + str(self.enigme_id) + " " + str(self.partie_id) + " : " + self.reponseDonnee


class Classe (db.Model):
    id = db.Column(db.Integer, primary_key=True)
    nom = db.Column(db.String(100))

    promo_id = db.Column(db.Integer,db.ForeignKey("promotion.id"))

    promotion = db.relationship("Promotion",backref=db.backref("classes",lazy="dynamic"))

    def __repr__(self):
        return "<Classe " + str(self.id) + " : " + self.nom + " " + str(self.promo_id)

class Etudiant(db.Model):

    mail = db.Column(db.String(200),db.ForeignKey("utilisateur.mail"), primary_key=True)
    promo_id = db.Column(db.Integer,db.ForeignKey("promotion.id"))
    classe_id = db.Column(db.Integer,db.ForeignKey("classe.id"))

    promotion = db.relationship("Promotion",backref=db.backref("etudiants",lazy="dynamic"))
    utilisateur = db.relationship("Utilisateur",backref=db.backref("etudiants",lazy="dynamic"))
    classe = db.relationship("Classe",backref=db.backref("etudiants",lazy="dynamic"))

    def __repr__(self):
        return self.utilisateur.nom + " " + self.utilisateur.prenom

class Obtention(db.Model):
    '''class correspondants aux notes des parties des étudiants'''
    note = db.Column(db.Float)

    partie_id = db.Column(db.Integer,db.ForeignKey("partie.id"), primary_key = True)
    etudiant_mail = db.Column(db.String(200),db.ForeignKey("etudiant.mail"), primary_key=True)

    partie = db.relationship("Partie",backref=db.backref("obtentions",lazy="dynamic"))
    etudiant = db.relationship("Etudiant",backref=db.backref("obtentions",lazy="dynamic"))

class Appartenir(db.Model):
    groupe_id = db.Column(db.Integer,db.ForeignKey("groupe.id"), primary_key=True)
    etudiant_mail = db.Column(db.String(200),db.ForeignKey("etudiant.mail"), primary_key=True)

    groupe = db.relationship("Groupe",backref=db.backref("appartenirs",lazy="dynamic"))
    etudiant = db.relationship("Etudiant",backref=db.backref("appartenirs",lazy="dynamic"))

class Accessible (db.Model):
    date = db.Column(db.Date())

    classe_id = db.Column(db.Integer,db.ForeignKey("classe.id"), primary_key=True)
    scenario_id = db.Column(db.Integer,db.ForeignKey("scenario.id"), primary_key=True)

    classe = db.relationship("Classe",backref=db.backref("accessibles",lazy="dynamic"))
    scenario = db.relationship("Scenario",backref=db.backref("accessibles",lazy="dynamic"))

    def __repr__(self):
        return "<Accessible le " + str(self.date)

class Avis(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    texteAvis = db.Column(db.String(500))
    date = db.Column(db.Date())
    star = db.Column(db.Float())

    utilisateur_mail = db.Column(db.String(200),db.ForeignKey("utilisateur.mail"))

    utilisateur = db.relationship("Utilisateur",backref=db.backref("avis",lazy="dynamic"))

def get_avis():
    return Avis.query.all()

def vote(etoile, comm, user):
    o = Avis(texteAvis = comm, star = etoile, utilisateur_mail = user, date = date.today())
    db.session.add(o)
    db.session.commit()

def moyenne_des_votes():
    avis = Avis.query.all()
    liste_avis = []
    for a in avis:
        liste_avis.append(a.star)
    if liste_avis != []:
        return round(sum(liste_avis)/len(liste_avis),1)
    return -1

@login_manager.user_loader
def load_user(username):
    return Utilisateur.query.get(username)