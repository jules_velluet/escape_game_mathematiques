# -*- coding: utf-8 -*-

import click
from .app import app, db
from datetime import datetime
from datetime import time
from .models import *
from hashlib import sha256


@app.cli.command()
@click.argument('filename')
def loaddb(filename):
    '''Creates the tables and populates them with data.'''
    # création de toutes les tables
    db.create_all()
    # chargement de notre jeu de données
    import yaml
    data = yaml.safe_load(open(filename))
    dictMatiere = {}
    dictSalle = {}
    dictScenario = {}
    dictIndice = {}
    dictGroupe = {}
    dictEnigme = {}
    dictPartie = {}
    for b in data:  
        if "promotions" in b:
            o = Promotion(nom = b["promotions"], etablissement = b["etablissement"])
        elif "indices" in b:
            o = Indice(texte = b["indices"])
            dictIndice[b["indices"]] = o
        elif "utilisateurs" in b:
            m = sha256()
            m.update(b["motdepasse"].encode())
            o = Utilisateur(nom = b["utilisateurs"], prenom = b["prenom"], mail = b["mail"], motDePasse = m.hexdigest(), imageProfil = b["imageprofil"])
        elif "groupes" in b:
            o = Groupe(nom=b["groupes"])
            dictGroupe[b["groupes"]] = o
        elif "matieres" in b:
            a = b["matieres"]
            o = Matiere(intitule = a)
            dictMatiere[a] = o
        elif "salles" in b:
            o = Salle(titre = b["salles"], image = b["image"])
            dictSalle[b["salles"]] = o
        elif "obtention" in b:
            o = Obtention(partie_id = b["partie"], etudiant_mail = b["etudiant"], note = b["obtention"])
        db.session.add(o)        
    db.session.commit()

    for b in data:
        if "scenarios" in b:
            o = Scenario(nom = b["scenarios"], salle_id = b["salle_id"], image = b["image"], description = b["description"], domaine = b["domaine"], matiere_id = dictMatiere[b["matiere"]].id, prof_mail = b["prof"], temps = time(b["temps"][0],b["temps"][1],b["temps"][2]))
            dictScenario[b["scenarios"]] = o
        elif "construire" in b:
            o = Construire(salle_id = dictSalle[b["salle"]].id, scenario_id = dictScenario[b["construire"]].id)
        elif "enigmes" in b:
            o = Enigme(titre = b["enigmes"], texte = b["texte"], reponse = b["reponse"], domaine = b["domaine"], indice_id = dictIndice[b["indice"]].id, salle_id = dictSalle[b["salle"]].id, matiere_id = dictMatiere[b["matiere"]].id, points = b["points"], image = b["image"])
            dictEnigme[b["enigmes"]] = o
        elif "parties" in b:
            o = Partie(finit = b["parties"], scenario_id = dictScenario[b["scenario"]].id, groupe_id = dictGroupe[b["groupe"]].id, timer = time(b["temps"][0],b["temps"][1],b["temps"][2]))
            dictPartie[o.id] = o
        elif "proposition" in b:
            o = Proposition(texte = b["proposition"], enigme_id = dictEnigme[b["enigme"]].id)
        elif "contient" in b:
            o = Contient(x = b["contient"], y = b["axey"], salle_id = dictSalle[b["salle"]].id, enigme_id = dictEnigme[b["enigme"]].id)
        elif "avancer" in b:
            o = Avancer(resolue = b["resolue"], reponseDonnee = b["avancer"], enigme_id = dictEnigme[b["enigme"]].id, partie_id = b["partie"])
        elif "classes" in b:
            o = Classe( nom = b["classes"], promo_id = b["promotion"])
        elif "accessible" in b:
            o = Accessible( scenario_id = b["accessible"], classe_id = b["classe"], date = datetime.strptime(b["date"], "%d-%m-%Y"))
        elif "etudiants" in b:
            o = Etudiant(mail = b["etudiants"], promo_id = b["promotion"], classe_id = b["classe"])
        elif "appartenir" in b:
            o = Appartenir(groupe_id = b["appartenir"], etudiant_mail = b["etudiant"])
        elif "professeurs" in b:
            o = Professeur(mail = b["professeurs"])
        elif "administrateurs" in b:
            o = Administrateur(mail = b["administrateurs"])
        elif "avis" in b:
            o = Avis(utilisateur_mail = b["utilisateur"], texteAvis = b["avis"], star = b["star"], date = datetime.strptime(b["date"], "%d-%m-%Y"))
        elif "suivante" in b:
            o = Suivre(salle_precedente_id = b["salle_pres"], salle_suivante_id = b["salle_suiv"], salle_actuelle_id = b["suivante"])
        db.session.add(o)
    db.session.commit()


@app.cli. command ()
@click.argument('mail')
@click.argument('password')
def newuser(mail, password):
    m = sha256()
    m.update(password.encode())
    u = Utilisateur(mail=mail,motDePasse=m.hexdigest())
    db.session.add(u)
    db.session.commit()