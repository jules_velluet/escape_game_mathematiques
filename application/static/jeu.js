var id_timer;

function myTimer() {
    var timer = document.getElementById('timer');
    var heureRestante = document.getElementsByClassName('heureRestante');
    var minuteRestante = document.getElementsByClassName('minuteRestante');
    var secondeRestante = document.getElementsByClassName('secondeRestante');

    if(seconde > 0){
        seconde -= 1;
    }else {
        seconde = 59;
        if(minute > 0){
            minute -= 1;
        }else{
            minute = 59;
            if(heure > 0){
                heure -= 1;
            }
        }
    }

    for(var i = 0; i<heureRestante.length; i++){
        heureRestante[i].value = heure;
        minuteRestante[i].value = minute;
        secondeRestante[i].value = seconde;
    }

    timer.textContent = heure + ":" + minute + ":" + seconde;

    if (heure == 0 && minute == 0 && seconde == 0){
        stopTimer();
        document.getElementById("perdu").style.display = "flex";
    } 
}

function cacher(o){
    if( o == "introduction"){
        id_timer = setInterval(myTimer, 1000);
    }
    document.getElementById(o).style.display = "none";
    document.getElementById("fondBlanc").style.display = "none";
    var enig = document.getElementsByClassName("enigme");
    for(var i = 0; i < enig.length; i++){
        enig[i].style.display = "block";
    }
}

function cacherIndice(){
    document.getElementById("indice").style.display = "none";
}

function afficheEnigme(text, titre, id_enigme, indice, img){
    document.getElementById("fondBlanc").style.display = "flex";
    document.getElementById("pageEnigme").style.display = "block";
    document.getElementById("textEnigme").textContent = text;
    document.getElementById("titreEnigme").textContent = titre;
    document.getElementById("enigme_id").value = id_enigme;
    document.getElementById("textIndice").textContent = indice;
    console.log(img);
    document.getElementById("img_enigme").src = img;
}

function afficheResultat(){
    document.getElementById('resultat').style.display = "flex";
}

function afficheIndice(){
    document.getElementById("indice").style.display = "flex";
}

function stopTimer(){
    clearInterval(id_timer);
}