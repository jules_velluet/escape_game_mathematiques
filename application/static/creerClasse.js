function creerClasse() {
  var btnPopup = document.getElementById('btnCreerClasse');
  var overlay = document.getElementById('overlay');
  overlay.style.display='block';
  // document.getElementById('classe').focus;
}

function ajouterEleve() {
  var btnPopup = document.getElementById('btnAjouterEleve');
  var ajoutEleve = document.getElementById('ajoutEleve');
  ajoutEleve.style.display='block';
}

function creerEleve(){
  var btnPopup = document.getElementById('btnCreerEleve');
  var creerEleve = document.getElementById('creerEleve');
  creerEleve.style.display='block';
}

function ajouterPlusieursEleve(){
  var btnPopup = document.getElementById('btnEleve');
  var creerEleve = document.getElementById('ajouterPlusieursEleve');
  creerEleve.style.display='block';
}

function suppEleve(prenom,nom,email){
  var btnPopup = document.getElementById('suppEleve');
  var creerEleve = document.getElementById('supprimerEleve');
  creerEleve.style.display='block';
  document.supprimer.prenomsupp.value = prenom;
  document.supprimer.nomsupp.value = nom;
  document.supprimer.mailsupp.value = email;
}

function suppClasse(classe){
  var btnPopup = document.getElementById('suppClasse');
  var creerEleve = document.getElementById('supprimerClasse');
  creerEleve.style.display='block';
  document.supprimerClasse.classesupp.value = classe;
}

function publier(ids){
  var overlay = document.getElementById('overlay');
  overlay.style.display='block';
  let hidden = document.getElementById('idS');
  hidden.value = ids;
}

function ajouterPromo(){
  var btnPopup = document.getElementById('btnpromo');
  var creerEleve = document.getElementById('ajouterpromo');
  creerEleve.style.display='block';
}

// function suppScenar(scena){
//   console.log(scena)
//   let suppScenare = document.getElementById('supprimerScenario');
//   document.supprimer.escape.value = scena;
// }

function liste_salles(){
  var select = document.getElementById("selectScenario")
  // var salle = select.options[select.selectedIndex].value.split(",");
}

function popUpStatistique(){
  var popup = document.getElementById("popupstat");
  popup.style.display='flex';
}

function cacherStatistique(){
  var popup = document.getElementById("popupstat");
  popup.style.display= "None";
}

function ajouterPlusieursEleveG(){
  var btnPopup = document.getElementById('btnEleveG');
  var creerEleve = document.getElementById('ajouterPlusieursEleveG');
  creerEleve.style.display='block';
}

function afficherEtu(classe){
  let div = document.getElementById(classe);
  let a = window.getComputedStyle(div, null)['display'];
  if (a == "none"){
     div.style.display = "contents";
  }
  else{
    div.style.display = "none";
  }
}

function voirNote(){
  var select = document.getElementById("selectScenario");
  var valeur = select.options[select.selectedIndex].value;
  var id = select.options[select.selectedIndex].id;
  var note = document.getElementById("note")
  var essaie = document.getElementById("essaie")
  var abandon = document.getElementById("abandon")
  valeur = valeur.replace("{","");
  valeur = valeur.replace("}","");
  valeur = valeur.split("],")
  for(elem in valeur){
    valeur[elem] = valeur[elem].replace("[","")
    valeur[elem] = valeur[elem].replace(" ","")
    valeur[elem] = valeur[elem].replace("'","")
    valeur[elem] = valeur[elem].replace("'","")
    valeur[elem] = valeur[elem].replace("]","")
    valeur[elem] = valeur[elem].split(":")
    for(e in valeur[elem]){
      valeur[elem][e] = valeur[elem][e].split(",")
    }
  }
  note.textContent = "Moyenne : " + valeur[parseInt(id)-1][1][0];
  essaie.textContent = "Nombre d'essaie : " + valeur[parseInt(id)-1][1][1];
  abandon.textContent = "Nombre d'abandon : " + valeur[parseInt(id-1)][1][2];
}

function creerSalle(){
  var select = document.getElementById('coucou');
  var valeur = select.options[select.selectedIndex].value
  var fifi = document.getElementById('fofo');
  var a = document.getElementsByClassName('titi')
  var adelete = document.getElementById("papadiv");
  var dsub = document.getElementById('submit');
  if (dsub != null){
    dsub.remove();
  }
  if(adelete!=null){
    adelete.remove();
  }
  var papadiv = document.createElement("div");
  papadiv.id = "papadiv";
  for (var i= 0; i<valeur;i++){
    var div = document.createElement("div");
    div.className = "profEleve";
    var p = document.createElement('p');
    p.textContent = "Salle numéro "+(i+1);
    div.appendChild(p);
    var titre = document.createElement('input');
    titre.setAttribute('type','text');
    titre.setAttribute('name','titre'+i);
    titre.setAttribute('class',"ideeajulien");
    titre.setAttribute('placeholder',"Donner un nom à votre salle")
    div.appendChild(titre);
    var img = document.createElement('input');
    img.setAttribute('type','file');
    img.setAttribute('name','img'+i);
    img.setAttribute('class',"ideeajulien")
    img.setAttribute('placeholder',"Choisir un lien d'une image");
    div.appendChild(img);
    papadiv.appendChild(div)
  }
  fifi.appendChild(papadiv);
  var submit = document.createElement('input');
  submit.setAttribute('type','submit');
  submit.setAttribute('value','Créer salle(s)');
  submit.setAttribute('class','boutonStat');
  submit.setAttribute('id','submit');
  fifi.appendChild(submit);
}

function dict_P_to_JS(dict){
  if (document.getElementById('cool') != null){
    let patrick = document.getElementById('cool');
    patrick.remove();
  }
  let liste_sc = dict.split("}");
  var res = new Map();
  let indice = 0;
  let indice_salle = 0;
  for (let a in dict){
      if (dict[a] == ":" && dict[a-1]!=" "){
        indice = parseInt(a)-1;
      }
      if (dict[a] == "["){
        indice_salle = parseInt(a)+1;
      }
      if (dict[a] == "]"){
        res.set(dict[indice],dict.substr(indice_salle,(parseInt(a)-5)));
      }
  }
  let liste_salle = [];
  for (var [key,value] of res){
    liste_salle.push(value.split(","));
    res.set(key,liste_salle);
  }
  var thomas = document.getElementById('pinsard');
  var f = document.getElementById('juju');
  if (document.getElementById('cool') == null){
    var combo = document.createElement('select');
    combo.setAttribute('id','cool');
    combo.setAttribute('name','select2');
    f.appendChild(combo);
  }
  for (let [key,value] of res){
    let kk = parseInt(key)-1
    if (key == thomas.selectedIndex.toString()){
      for (let j = 0; j<value[kk].length;++j){
        var element = document.createElement("option");
        element.setAttribute("name",value[kk][j]);
        element.setAttribute("value",j+1);
        element.text = value[kk][j];
        combo.appendChild(element);
      }
    }
  }
}

function groupe(){
  var select = document.getElementById('select_groupe');
  var alert = document.getElementById('alert_groupe');
  var classes = document.getElementsByClassName('groupe')
  if (select.options[select.selectedIndex].textContent != "Choisis ton groupe"){
    cacher('alert_groupe')
  }else{
    alert.style.display = "block"
  }
  for(var i = 0; i<classes.length; i++){
    classes[i].value = select.options[select.selectedIndex].value;
  }
}

function popup_creer_groupe(){
  document.getElementById("creer_groupe").style.display = "flex";
}

function popup_ajouter_eleves(){
  document.getElementById("ajouter_eleves").style.display = "flex";
}

function affiche_eleve(param, idGroupe, nomGroupe){
  var ul = document.getElementById("eleve_in_groupe");
  document.getElementById("groupe_etu").value = idGroupe;
  document.getElementById("groupe_choisis").textContent = "Etudiants dans le groupe : " + nomGroupe;
  ul.innerHTML=""
  param = param.replace("[","");
  param = param.replace("]","");
  param = param.split(",")
  for(var i=0; i<param.length; i++){
    var li = document.createElement("li")
    li.textContent = param[i]
    ul.appendChild(li);
  }
}

function fermer_ajout_eleve(){
  document.getElementById("ajouter_eleves").style.display = "None"
}

function cacher(o){
  document.getElementById(o).style.display = "None"
}

function popUpStatistiqueClasse(id){
  let popup = document.getElementById("popupstatclasse"+id);
  popup.style.display = 'flex';
}

function voter(){
  // On va chercher toutes les étoiles
  const stars = document.querySelectorAll(".star_vote");
  // On va chercher l'input
  const note = document.querySelector("#note");

  // On boucle sur les étoiles pour le ajouter des écouteurs d'évènements
  for(star of stars){
      // On écoute le survol
      star.addEventListener("mouseover", function(){
          resetStars();
          this.style.color = "red";
          this.classList.add("las");
          this.classList.remove("lar");
          // L'élément précédent dans le DOM (de même niveau, balise soeur)
          let previousStar = this.previousElementSibling;

          while(previousStar){
              // On passe l'étoile qui précède en rouge
              previousStar.style.color = "red";
              previousStar.classList.add("las");
              previousStar.classList.remove("lar");
              // On récupère l'étoile qui la précède
              previousStar = previousStar.previousElementSibling;
          }
      });

      // On écoute le clic
      star.addEventListener("click", function(){
          note.value = this.dataset.value;
      });

      star.addEventListener("mouseout", function(){
          resetStars(note.value);
      });
  }

  /**
   * Reset des étoiles en vérifiant la note dans l'input caché
   * @param {number} note 
   */
  function resetStars(note = 0){
      for(star of stars){
          if(star.dataset.value > note){
              star.style.color = "black";
              star.classList.add("lar");
              star.classList.remove("las");
          }else{
              star.style.color = "red";
              star.classList.add("las");
              star.classList.remove("lar");
          }
      }
  }
}